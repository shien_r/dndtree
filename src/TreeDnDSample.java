
//-*- mode:java; encoding:utf-8 -*-
// vim:set fileencoding=utf-8:
//@homepage@
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.io.*;
import javax.swing.*;
import javax.swing.tree.*;

public class TreeDnDSample extends JPanel {
    public TreeDnDSample() {
        super(new BorderLayout());
        DefaultTreeModel model = makeModel();
        DnDTree tree = new DnDTree();
        tree.setModel(model);
        for(int i=0;i<tree.getRowCount();i++) {
            tree.expandRow(i);
        }
        add(new JScrollPane(tree));
        setPreferredSize(new Dimension(320, 240));
    }
    private DefaultTreeModel makeModel() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
        DefaultMutableTreeNode set1 = new DefaultMutableTreeNode("Set 001");
        DefaultMutableTreeNode set2 = new DefaultMutableTreeNode("Set 002");
        DefaultMutableTreeNode set3 = new DefaultMutableTreeNode("Set 003");
        set1.add(new DefaultMutableTreeNode("111111111"));
        set1.add(new DefaultMutableTreeNode("22222222222"));
        set1.add(new DefaultMutableTreeNode("33333"));
        set2.add(new DefaultMutableTreeNode("asdfasdfas"));
        set2.add(new DefaultMutableTreeNode("asdf"));
        set3.add(new DefaultMutableTreeNode("asdfasdfasdf"));
        set3.add(new DefaultMutableTreeNode("5555555555"));
        set3.add(new DefaultMutableTreeNode("66666666666666"));
        root.add(set1);
        root.add(set2);
        set2.add(set3);
        return new DefaultTreeModel(root);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override public void run() {
                createAndShowGUI();
            }
        });
    }
    public static void createAndShowGUI() {
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e) {
            e.printStackTrace();
        }
        JFrame frame = new JFrame("@title@");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(new TreeDnDSample());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}

//Java Swing Hacks - HACK #26: DnD JTree
//http://www.oreilly.co.jp/books/4873112788/
class DnDTree extends JTree {
    //private DragSource dragSource;
    //private DropTarget dropTarget;
    private TreeNode dropTargetNode = null;
    private TreeNode draggedNode = null;

    public DnDTree() {
        super();
                
        TreeDropAdapter hoge = new TreeDropAdapter(this);
        new TreeDragAdapter(this);
        DragGestureAdapter dga = new DragGestureAdapter(this);
        new DragSource().createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, dga);
        
        setCellRenderer(new DnDTreeCellRenderer());
        setModel(new DefaultTreeModel(new DefaultMutableTreeNode("default")));
        //dragSource = new DragSource();
        //DragGestureReco0gnizer dgr =
        //dropTarget =
        new DropTarget(this,hoge);
    }

    // custom renderer
    class DnDTreeCellRenderer extends DefaultTreeCellRenderer {
        private static final int BOTTOM_PAD = 30;
        private boolean isTargetNode;
        private boolean isTargetNodeLeaf;
        private boolean isLastItem;
        private Insets normalInsets;
        private Insets lastItemInsets;
        public DnDTreeCellRenderer() {
            super();
            normalInsets = super.getInsets();
            lastItemInsets = new Insets(
                normalInsets.top,
                normalInsets.left,
                normalInsets.bottom + BOTTOM_PAD,
                normalInsets.right);
        }
        @Override public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                      boolean isSelected, boolean isExpanded, boolean isLeaf,
                                                      int row, boolean hasFocus) {
            isTargetNode = value == dropTargetNode;
            isTargetNodeLeaf = isTargetNode && ((TreeNode)value).isLeaf();
            // isLastItem = (index == list.getModel().getSize()-1);
            //boolean showSelected = isSelected & (dropTargetNode == null);
            return super.getTreeCellRendererComponent(tree, value, isSelected, isExpanded, isLeaf, row, hasFocus);
        }
        @Override public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if(isTargetNode) {
                g.setColor(Color.BLACK);
                if(isTargetNodeLeaf) {
                    g.drawLine(0, 0, getSize().width, 0);
                }else{
                    g.drawRect(0, 0, getSize().width-1, getSize().height-1);
                }
            }
        }
    }

	public void initNode() {
		this.dropTargetNode=null;
		this.draggedNode=null;
	}

	public void updateDraggedNode(TreeNode lastPathComponent) {
		draggedNode = lastPathComponent;
	}

	public TreeNode getDraggedNode() {
		return draggedNode;
	}

	public void updateDropNode(DefaultMutableTreeNode targetNode) {
		dropTargetNode = targetNode;
		
	}
}