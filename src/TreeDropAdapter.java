import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;


public class TreeDropAdapter implements DropTargetListener {
	private static final String NAME = "TREE-TEST";
	DnDTree dndTree;
	
	TreeDropAdapter(DnDTree _dndTree) {
		dndTree = _dndTree;
	}

	  // DropTargetListener ---->
    @Override public void dropActionChanged(DropTargetDragEvent dtde) {}
    @Override public void dragEnter(DropTargetDragEvent dtde) {}
    @Override public void dragExit(DropTargetEvent dte) {}
    
    @Override public void dragOver(DropTargetDragEvent dtde) {
        DataFlavor[] f = dtde.getCurrentDataFlavors();
        boolean isDataFlavorSupported = f[0].getHumanPresentableName().equals(NAME);
        if(!isDataFlavorSupported) {
            //ã‚µãƒãƒ¼ãƒˆã•ã‚Œã¦ã„ãªã„DataFlavorã§ã‚ã‚‹(ä¾‹ãˆã°ãƒ‡ã‚¹ã‚¯ãƒˆãƒƒãƒ—ã‹ã‚‰ãƒ•ã‚¡ã‚¤ãƒ«ãªã©)
            rejectDrag(dtde);
            return;
        }
        // figure out which cell it's over, no drag to self
        Point pt = dtde.getLocation();
        TreePath path = dndTree.getPathForLocation(pt.x, pt.y);
        if(path==null) {
            //ãƒŽãƒ¼ãƒ‰ä»¥å¤–ã®å ´æ‰€ã§ã‚ã‚‹(ä¾‹ãˆã°JTreeã®ä½™ç™½ãªã©)
            rejectDrag(dtde);
            return;
        }
//         Object draggingObject;
//         if(!isWebStart()) {
//             try{
//                 draggingObject = dtde.getTransferable().getTransferData(localObjectFlavor);
//             }catch(Exception ex) {
//                 rejectDrag(dtde);
//                 return;
//             }
//         }else{
//             draggingObject = getSelectionPath().getLastPathComponent();
//         }
        Object draggingObject             = dndTree.getSelectionPath().getLastPathComponent();
        MutableTreeNode draggingNode      = (MutableTreeNode) draggingObject;
        DefaultMutableTreeNode targetNode = (DefaultMutableTreeNode) path.getLastPathComponent();
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) targetNode.getParent();
        while(parentNode!=null) {
            if(draggingNode.equals(parentNode)) {
                //è¦ªãƒŽãƒ¼ãƒ‰ã‚’å­ãƒŽãƒ¼ãƒ‰ã«ãƒ‰ãƒ­ãƒƒãƒ—ã—ã‚ˆã†ã¨ã—ã¦ã„ã‚‹
                rejectDrag(dtde);
                return;
            }
            parentNode = (DefaultMutableTreeNode)parentNode.getParent();
        }
        //dropTargetNode ã¯ã€æç”»ç”¨(Rectangle2Dã€Line)ã®flag
        dndTree.updateDropNode(targetNode);
        dtde.acceptDrag(dtde.getDropAction());
        dndTree.repaint();
    }
    @Override public void drop(DropTargetDropEvent dtde) {
        //System.out.println("drop");
//         if(!isWebStart()) {
//             try{
//                 draggingObject = dtde.getTransferable().getTransferData(localObjectFlavor);
//             }catch(Exception ex) {
//                 rejectDrag(dtde);
//                 return;
//             }
//         }else{
//             draggingObject = getSelectionPath().getLastPathComponent();
//         }
        Object draggingObject  = dndTree.getSelectionPath().getLastPathComponent();
        DefaultTreeModel model = (DefaultTreeModel) dndTree.getModel();
        Point p = dtde.getLocation();
        TreePath path = dndTree.getPathForLocation(p.x, p.y);
        if(path==null || !(draggingObject instanceof MutableTreeNode)) {
            dtde.dropComplete(false);
            return;
        }
        //System.out.println("drop path is " + path);
        MutableTreeNode draggingNode      = (MutableTreeNode) draggingObject;
        DefaultMutableTreeNode targetNode = (DefaultMutableTreeNode) path.getLastPathComponent();
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) targetNode.getParent();
        if(targetNode.equals(draggingNode)) {
            //è‡ªåˆ†ã‚’è‡ªåˆ†ã«ã¯ãƒ‰ãƒ­ãƒƒãƒ—ä¸å¯
            dtde.dropComplete(false);
            return;
        }
        dtde.acceptDrop(DnDConstants.ACTION_MOVE);
        model.removeNodeFromParent(draggingNode);
        if(parentNode!=null && targetNode.isLeaf()) {
            model.insertNodeInto(draggingNode, parentNode, parentNode.getIndex(targetNode));
        }else{
            model.insertNodeInto(draggingNode, targetNode, targetNode.getChildCount());
        }
        dtde.dropComplete(true);
    }
    private void rejectDrag(DropTargetDragEvent dtde) {
        dtde.rejectDrag();
        dndTree.updateDropNode(null);
        dndTree.repaint();             // Rectangle2Dã€Lineã‚’æ¶ˆã™ãŸã‚JTreeã‚’å†æç”»
    }
    // <---- DropTargetListener
}
