import java.awt.Cursor;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;



public class DragGestureAdapter implements DragGestureListener {
	private DnDTree dndTree;
	
	DragGestureAdapter(DnDTree _dndTree) {
		dndTree = _dndTree;
	}
	
    // DragGestureListener ---->
    @Override public void dragGestureRecognized(DragGestureEvent dge) {
        //System.out.println("dragGestureRecognized");
        Point pt = dge.getDragOrigin();
        TreePath path = dndTree.getPathForLocation(pt.x, pt.y);
        if(path==null || path.getParentPath()==null) {
            return;
        }
        //System.out.println("start "+path.toString());
        dndTree.updateDraggedNode((TreeNode)path.getLastPathComponent());
        
        Transferable trans = new RJLTransferable(dndTree.getDraggedNode());
        dge.startDrag(Cursor.getDefaultCursor(), trans);
        //new DragSource().startDrag(Cursor.getDefaultCursor(), trans);
    }
    // <---- DragGestureListener

	public void createDefaultDragGestureRecognizer(DnDTree dnDTree2,
			int actionMove, DnDTree dnDTree3) {
		// TODO Auto-generated method stub
		
	}


}
