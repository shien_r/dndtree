import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;


public class RJLTransferable implements Transferable {
	private static final String NAME = "TREE-TEST";
    private static final DataFlavor localObjectFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType, NAME);
	private static final DataFlavor[] supportedFlavors = { localObjectFlavor };
	Object object;
    public RJLTransferable(Object o) {
        object = o;
    }
    @Override public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
        if(isDataFlavorSupported(df)) {
            return object;
        }else{
            throw new UnsupportedFlavorException(df);
        }
    }
    @Override public boolean isDataFlavorSupported(DataFlavor df) {
        return df.getHumanPresentableName().equals(NAME);
        //return (df.equals(localObjectFlavor));
    }
    @Override public DataFlavor[] getTransferDataFlavors() {
        return supportedFlavors;
    }

}
