import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;

public class TreeDragAdapter implements DragSourceListener {
    private DnDTree dndTree;

	public TreeDragAdapter(DnDTree _dndTree) {
	    dndTree= _dndTree;
	}

	   // DragSourceListener ---->
    @Override public void dragDropEnd(DragSourceDropEvent dsde) {
        dndTree.initNode();
        dndTree.repaint();
    }
    @Override public void dragEnter(DragSourceDragEvent dsde) {
        dsde.getDragSourceContext().setCursor(DragSource.DefaultMoveDrop);
    }
    @Override public void dragExit(DragSourceEvent dse) {
        dse.getDragSourceContext().setCursor(DragSource.DefaultMoveNoDrop);
    }
    @Override public void dragOver(DragSourceDragEvent dsde) {}
    @Override public void dropActionChanged(DragSourceDragEvent dsde) {}
    // <---- DragSourceListener

}
